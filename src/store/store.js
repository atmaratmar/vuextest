import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
//after export now we have to improt in main.js
export const store = new Vuex.Store({
    state:{
        products:[
            {name:'Atmar', id:1},
            {name:'Mursal', id:2},
            {name:'Jaz', id:3},
            {name:'Anaya', id:4},
            {name:'Anaya2', id:5}
          ]
    },
    getters:{  
        // this we define our getters if change in one place it will change everywhere
    saleProducts:state=>{
        var saleProducts = state.products.map(product=>{
            return{
                name : '**'+ product.name,
                id : product.id 
                
            }
        });
        return saleProducts;
    }
    },
    mutations:{
        counter:state=>{
               
            state.products.find(x => x.id === 1).name="hdsjdhsh";
            //     state.products.forEach(product=>{
            //         product.name+"hello";
            //    })         
        }
    },

    actions:{
        // action take context
        counter: context=>{
            setTimeout(function(){
           // here we put the mutation by using context
           context.commit('counter')
            },2)
        }
    }
    
})